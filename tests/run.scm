(use posix data-structures)

(system "chmod +x lowdown-run")
(system (string-intersperse
         '("/usr/bin/env" "perl"
           "./MarkdownTest_1.0.3/MarkdownTest.pl"
           "--tidy"
           "--script" "./lowdown-run"
           "--testdir" "MarkdownTest_1.0.3/Tests")))

